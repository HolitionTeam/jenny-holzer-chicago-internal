// Copyright (c) 2019 8th Wall, Inc.
//
// app.js is the main entry point for your 8th Wall app. Code here will execute after head.html
// is loaded, and before body.html is loaded.

import {pipelineModule1} from './movement1.js'
import {pipelineModule2} from './movement2.js'
import {pipelineModule3} from './movement3.js'
import {pipelineModule4} from './movement4.js'
import {setupScreenshotPreview} from './screenshot.js'
import {texts} from './texts.js'

const onxrloaded = () => {
  const pipelineModules = [pipelineModule1, pipelineModule2, pipelineModule3, pipelineModule4]

  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  const id = urlParams.get('id') || '010'
  console.log(`Id ${id}`)
  const movementId = texts[id].movement
  const pipelineModule = pipelineModules[movementId-1]
  const lines = texts[id].text.split('/')
  
  XR8.addCameraPipelineModules([  // Add camera pipeline modules.
    // Existing pipeline modules.
    XR8.GlTextureRenderer.pipelineModule(),       // Draws the camera feed.
    XR8.Threejs.pipelineModule(),                 // Creates a ThreeJS AR Scene.
    XR8.XrController.pipelineModule(),            // Enables SLAM tracking.
    XR8.CanvasScreenshot.pipelineModule(),
    XRExtras.AlmostThere.pipelineModule(),       // Detects unsupported browsers and gives hints.
    XRExtras.FullWindowCanvas.pipelineModule(),  // Modifies the canvas to fill the window.
    XRExtras.Loading.pipelineModule(),           // Manages the loading screen on startup.
    XRExtras.RuntimeError.pipelineModule(),      // Shows an error image on runtime error.
    // Custom pipeline modules.
    pipelineModule(lines),                   // Sets up the threejs camera and scene content.
  ])

  setupScreenshotPreview()
  
  // Add a canvas to the document for our xr scene.
  const canvas = document.createElement('canvas')
  canvas.setAttribute('id', 'camerafeed')
  document.body.appendChild(canvas)

  XR8.CanvasScreenshot.configure({ jpgCompression: 90 })

  // Open the camera and start running the camera run loop.
  XR8.run({canvas})
}

window.XR8 ? onxrloaded() : window.addEventListener('xrloaded', onxrloaded)