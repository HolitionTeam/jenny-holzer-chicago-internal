import {loadFont, textRenderer} from './text-renderer.js'

export const pipelineModule2 = (textLines) => {
  const BACKGROUND_OPACITY = 0.3
  const BACKGROUND_BRIGHTNESS = 2.0
  const TEXT_OPACITY = 0.2
  const REFLECT_OPACITY = 0.3
  
  const TEXT_WIDTH = 5
  const TEXT_DEPTH = -5
  const INITIAL_HEIGHT = -TEXT_WIDTH / 2
  const FINAL_HEIGHT = 2
  const TWEEN_DURATION = 10000
  
  const LOOP_INTERVAL = 1000

  const lines = textLines

  const maskVertexShader =
    `
      varying vec2 vUv;
  
      void main() {
      	vUv = uv;
      	gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
      }
    `
  
  const maskFragmentShader = 
    ` precision mediump float;
      varying vec2 vUv;
      
      uniform bool flipUv;
      
      float remap(float val, float minInput, float maxInput, float minOutput, float maxOutput) {
        return minOutput + (maxOutput - minOutput) * ((val - minInput) / (maxInput - minInput));
      }

      void main() {
        float uv = flipUv ? 1.0 - vUv.y : vUv.y;
        float opacity = 1.0 - clamp(remap(uv, 0.5, 0.6, 0.0, 1.0), 0.0, 1.0);
        gl_FragColor = vec4(0.0, 0.0, 0.0, opacity);
      }
    `

  const cameraShader = 
  ` precision mediump float;

    varying vec2 texUv;
    uniform sampler2D sampler;
    uniform sampler2D textOverlay;
    uniform sampler2D textOverlayReflect;
    uniform float backgroundOpacity;
    uniform float backgroundBrightness;
    uniform float textOpacity;
    uniform float reflectOpacity;

    void main() {
      vec4 cameraSample = texture2D(sampler, texUv);
      vec4 overlaySample = texture2D(textOverlay, vec2(texUv.x, 1.0-texUv.y));
      vec4 reflectSample = texture2D(textOverlayReflect, vec2(texUv.x, 1.0-texUv.y));
      
      vec3 darkened = mix(vec3(0.0), cameraSample.rgb, backgroundOpacity);
      vec3 ligthened = cameraSample.rgb * backgroundBrightness;
      
      vec3 overlayLuminance = overlaySample.rgb;
      vec3 rgb = mix(darkened, ligthened, overlayLuminance) + overlayLuminance * textOpacity;

      rgb += reflectSample.rgb * reflectOpacity;

      gl_FragColor = vec4(rgb, 1.0);
    }`
    
  let textScene1 = null
  let textScene2 = null
  let renderTarget1 = null
  let renderTarget2 = null
  let textRender = null
  let textRenderReflect = null
  let initDone = false

  const initXrScene = (scene, camera, renderer, canvases) => {
    // Enable shadows in the rednerer.
    // renderer.shadowMap.enabled = true
    
    renderer.sortObjects = false

    XR8.GlTextureRenderer.configure({fragmentSource: cameraShader})

    // Load Textures
    ////////////////

    const textTexture = new THREE.CanvasTexture(canvases.text)
    const reflectTexture = new THREE.CanvasTexture(canvases.reflect)
    reflectTexture.flipY = false

    const width = window.innerWidth
    const height = window.innerHeight
    const pixelRatio = window.devicePixelRatio

    const effectiveWidth = width * pixelRatio
    const effectiveHeight = height * pixelRatio

    console.log(`Render Dimensions: ${width}x${height}@x${pixelRatio}`)
    
    // Main Text
    ////////////

    const textMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      map: textTexture,
      color: new THREE.Color(0xFFFFFF),
      transparent: true,
    })
    
		const textGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH)
    const textMesh = new THREE.Mesh(textGeometry, textMaterial)
    textMesh.position.set(0, INITIAL_HEIGHT, TEXT_DEPTH)

    // Text Mask
    ////////////
    
    const textMaskMaterial = new THREE.ShaderMaterial({
      vertexShader: maskVertexShader,
      fragmentShader: maskFragmentShader,
      side: THREE.DoubleSide,
      depthTest: false,
      transparent: true,
      uniforms: { flipUv: { value: false } }
    })
    
		const textMaskGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH * 2)
    const textMaskMesh = new THREE.Mesh(textMaskGeometry, textMaskMaterial)
    textMaskMesh.position.set(0, 0, TEXT_DEPTH)

    // Reflection
    /////////////

    const reflectMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      map: reflectTexture,
      transparent: true,
      color: new THREE.Color(0xFFFFFF)
    })

    const reflectGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH)
    reflectGeometry.rotateX(-Math.PI / 2)

    const reflectMesh = new THREE.Mesh(reflectGeometry, reflectMaterial)
    reflectMesh.position.set(0, 0, INITIAL_HEIGHT + TEXT_DEPTH)

    // Reflect Mask
    ///////////////
    
    const reflectMaskMaterial = new THREE.ShaderMaterial({
      vertexShader: maskVertexShader,
      fragmentShader: maskFragmentShader,
      side: THREE.DoubleSide,
      depthTest: false,
      transparent: true,
      uniforms: { flipUv: { value: true } }
    })
    
		const reflectMaskGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH * 2)
		reflectMaskGeometry.rotateX(-Math.PI / 2)
		
    const reflectMaskMesh = new THREE.Mesh(reflectMaskGeometry, reflectMaskMaterial)
    reflectMaskMesh.position.set(0, 0, TEXT_DEPTH)
    
    // Render Target
    ////////////////

    const rtOptions = {
      wrapS: THREE.ClampToEdgeWrapping,
      wrapT: THREE.ClampToEdgeWrapping,
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      type: THREE.UnsignedByteType,
      depthBuffer: false,
      stencilBuffer: false,
      generateMipmaps: false,
      shareDepthFrom: null,
    };

    renderTarget1 = new THREE.WebGLRenderTarget(effectiveWidth, effectiveHeight, rtOptions)
    renderTarget2 = new THREE.WebGLRenderTarget(effectiveWidth, effectiveHeight, rtOptions)

    textScene1 = new THREE.Scene()
    textScene2 = new THREE.Scene()
		

    // Set the initial camera position relative to the scene we just laid out. This must be at a
    // height greater than y=0.
    camera.position.set(0, 2, 2)
    
    textScene1.add(textMesh)
    textScene1.add(textMaskMesh)
    textScene2.add(reflectMesh)
    textScene2.add(reflectMaskMesh)
    animateIn(textMesh, reflectMesh)
  }

  const animateIn = (textMesh, reflectMesh) => {
    const startPosition = { position: INITIAL_HEIGHT }
    const tween = new TWEEN.Tween(startPosition)
      .to({ position: FINAL_HEIGHT }, TWEEN_DURATION)
      .easing(TWEEN.Easing.Quadratic.Out)
      .onStart(() => {
        textMesh.material.opacity = 1.0
        reflectMesh.material.opacity = 1.0
      })
      .onUpdate(() => {
        textMesh.position.y = startPosition.position
        reflectMesh.position.z = startPosition.position + TEXT_DEPTH
      })

    const startOpacity = { opacity: 1.0 }
    const opacityTween = new TWEEN.Tween(startOpacity)
      .to({ opacity: 0.0 }, 500)
      .delay(1000)
      .easing(TWEEN.Easing.Linear.None)
      .onUpdate(() => {
        textMesh.material.opacity = startOpacity.opacity
        reflectMesh.material.opacity = startOpacity.opacity
      })
      .onComplete(() => {
        setTimeout(() => { animateIn(textMesh, reflectMesh) }, LOOP_INTERVAL);
      })

    tween.chain(opacityTween)
    tween.start()
  }

  const renderText = () => {
    const renderParams = {
      fontFamily: 'FuturaCondensedBold',
      lineHeight: 1,
    }
    renderParams.fontSize = textRender.calcFontFize(lines, {
      fontFamiliy: renderParams.fontFamily,
      lineHeight: renderParams.lineHeight,
      margin: 0.1})
      
    textRender.render(lines, renderParams)
    textRender.blur(Math.round(renderParams.fontSize / 12))
    textRender.render(lines, renderParams)

    textRenderReflect.render(lines, renderParams)
    textRenderReflect.blur(40)
  }
  
  // Return a camera pipeline module that adds scene elements on start.
  return {
    // Camera pipeline modules need a name. It can be whatever you want but must be unique within
    // your app.
    name: 'threejsinitscene',

    // onStart is called once when the camera feed begins. In this case, we need to wait for the
    // XR8.Threejs scene to be ready before we can access it to add content. It was created in
    // XR8.Threejs.pipelineModule()'s onStart method.
    onStart: ({canvas}) => {
      const {scene, camera, renderer} = XR8.Threejs.xrScene() // Get the 3js scene from XR8.Threejs.
			
			loadFont(() => {
			  const res = 1024
			  textRender = textRenderer(res, res)
        textRenderReflect = textRenderer(res, res)
        const canvases = {
          text: textRender.canvas,
          reflect: textRenderReflect.canvas }
        renderText()
        initXrScene(scene, camera, renderer, canvases)
        initDone = true
      })

      // Enable TWEEN animations.
      animate()
      function animate(time) {
        requestAnimationFrame(animate)
        TWEEN.update(time)
      }
      
      // Sync the xr controller's 6DoF position and camera paremeters with our scene.
      XR8.XrController.updateCameraProjectionMatrix(
        {origin: camera.position, facing: camera.quaternion})
      
      // Recenter content when the canvas is tapped.
      // canvas.addEventListener(
      //  'touchstart', (e) => { e.touches.length == 1 && XR8.XrController.recenter() }, true)
    },
    
    onUpdate: ({frameStartResult, processGpuResult}) => {
      if (initDone) {
        const {scene, camera, renderer} = XR8.Threejs.xrScene()

        const currentTarget = renderer.getRenderTarget()
        renderer.setClearColor(new THREE.Color(0x000000), 0.0);
        renderer.setRenderTarget(renderTarget1)
        renderer.clear();
        renderer.render(textScene1, camera)
        renderer.setRenderTarget(renderTarget2)
        renderer.clear();
        renderer.render(textScene2, camera)
        renderer.setRenderTarget(currentTarget);
        
        // Configure Camera Shader
        
        const {shader} = processGpuResult.gltexturerenderer
        const {GLctx} = frameStartResult
        const p = XR8.GlTextureRenderer.getGLctxParameters(GLctx, [GLctx.TEXTURE0])
        GLctx.useProgram(shader)
        
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'backgroundOpacity'), BACKGROUND_OPACITY)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'backgroundBrightness'), BACKGROUND_BRIGHTNESS)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'textOpacity'), TEXT_OPACITY)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'reflectOpacity'), REFLECT_OPACITY)
        
        const textureProperties1 = renderer.properties.get(renderTarget1.texture);
        GLctx.activeTexture(GLctx.TEXTURE1);
        GLctx.bindTexture(GLctx.TEXTURE_2D, textureProperties1.__webglTexture);
        GLctx.uniform1i(GLctx.getUniformLocation(shader, 'textOverlay'), 1);

        const textureProperties2 = renderer.properties.get(renderTarget2.texture);
        GLctx.activeTexture(GLctx.TEXTURE2);
        GLctx.bindTexture(GLctx.TEXTURE_2D, textureProperties2.__webglTexture);
        GLctx.uniform1i(GLctx.getUniformLocation(shader, 'textOverlayReflect'), 2);
        
        XR8.GlTextureRenderer.setGLctxParameters(GLctx, p)
      }
    }
  }
}
