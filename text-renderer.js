const FONT_FILE = './assets/Futura_Condensed_Bold.otf'

export const textRenderer = (width, height) => {
  const canvasWidth = width
  const canvasHeight = height
  const canvas = document.createElement('canvas')
  canvas.width = canvasWidth
  canvas.height = canvasHeight
  const ctx = canvas.getContext('2d')

  const renderLines = (lines, params) => {
    const lineHeight = params.fontSize * params.lineHeight
    let y = params.y ? params.y : (canvasHeight - lineHeight * (lines.length - 1)) / 2
    for (const line of lines) {
      const metrics = ctx.measureText(line)
      const x = params.x ? params.x : (canvasWidth - metrics.width) / 2
      ctx.fillText(line, x, y)
      y += lineHeight
    }
  }
      
  return {
    ctx,
    canvas,

    render: (lines, params) => {
      ctx.font = `${params.fontSize}px ${params.fontFamily}`
      ctx.fillStyle = 'white'
    
      renderLines(lines, params)
    },

    blur: (radius) => {
      StackBlur.canvasRGBA(canvas, 0, 0, canvasWidth, canvasHeight, radius)
    },

    clear: () => {
      ctx.clearRect(0, 0, canvasWidth, canvasHeight);
    },
    
    calcFontFize: (lines, params) => {
      const desiredWidth = canvasWidth - (canvasWidth * params.margin * 2)
      const desiredHeight = canvasHeight - (canvasHeight * params.margin * 2)

      const refFontSize = 100
      ctx.font = `${refFontSize}px ${params.fontFamily}`

      let maxWidth = 0
      for (const line of lines) {
        const width = ctx.measureText(line).width
        maxWidth = Math.max(maxWidth, width)
      }
      const maxHeight = lines.length * params.lineHeight * refFontSize
      const fontSizeX = desiredWidth * refFontSize / maxWidth
      const fontSizeY = desiredHeight * refFontSize / maxHeight
      return Math.round(Math.min(fontSizeX, fontSizeY))
    }
  }
}

export const loadFont = (callback) => {
    const fontFace = new FontFace('FuturaCondensedBold', `url(${FONT_FILE})`, {})
    document.fonts.add(fontFace)
    const promise = fontFace.load()
    promise.then(callback)
    promise.catch(error => console(error.message));
}

export const renderTextSplit = (lines) => {
  const res = 1024
  
  const renderParams = {
    fontFamily: 'FuturaCondensedBold',
    lineHeight: 1,
    margin: 0.1,
  }
  const canvases = { text: [], reflect: [] }

  let y = null
  for (const line of lines) {
    const segments = line.split(',')

    let x = null
    for (const segment of segments) {
      const textRender = textRenderer(res, res)
      const textRenderReflect = textRenderer(res, res)
      canvases.text.push(textRender.canvas)
      canvases.reflect.push(textRenderReflect.canvas)

      if (x === null) {
        renderParams.fontSize = textRender.calcFontFize(lines, {
          fontFamiliy: renderParams.fontFamily,
          lineHeight: renderParams.lineHeight,
          margin: renderParams.margin})

        textRender.ctx.font = `${renderParams.fontSize}px ${renderParams.fontFamily}`
        const lineMetrics = textRender.ctx.measureText(line.replaceAll(',', ' '))
        x = (res - lineMetrics.width) / 2
      }
      if (y === null) {
        const lineHeight = renderParams.fontSize * renderParams.lineHeight
        y = (res - lineHeight * (lines.length - 1)) / 2
      }
      renderParams.x = x
      renderParams.y = y

      textRender.render([segment], renderParams)
      textRender.blur(Math.round(renderParams.fontSize / 12))
      textRender.render([segment], renderParams)
      
      textRenderReflect.render([segment], renderParams)
      textRenderReflect.blur(40)

      x += textRender.ctx.measureText(segment).width
    }

    y += renderParams.fontSize * renderParams.lineHeight
  }
  console.log(`Num canvases ${canvases.text.length}`)
  return canvases
}

