const takeScreenshot = () => {
  XR8.canvasScreenshot().takeScreenshot().then((data) => {
    console.log('Capture screenshot')
    const image = document.getElementById('screenshot-preview')
    image.src = 'data:image/jpeg;base64,' + data
    image.style.display = 'block'
  })
}

const hidePreview = () => {
  const image = document.getElementById('screenshot-preview')
  image.style.display = 'none'
}

export const setupScreenshotPreview = () => {
  const image = document.createElement('img')
  image.setAttribute('id', 'screenshot-preview')
  image.width = window.innerWidth / 2
  image.height = window.innerHeight / 2
  image.style.left = `${(window.innerWidth - image.width) / 2}px`
  image.addEventListener('touchstart', hidePreview)
  document.body.appendChild(image)

  const button = document.createElement('div')
  button.setAttribute('id', 'screenshot-button')
  button.addEventListener('touchstart', takeScreenshot)
  button.style.left = `${(window.innerWidth - 74) / 2}px`
  document.body.appendChild(button)
}
