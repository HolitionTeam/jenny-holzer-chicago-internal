import {loadFont, textRenderer} from './text-renderer.js'
import {CustomEase} from './custom-ease.js'

export const pipelineModule4 = (textLines) => {
  const BACKGROUND_OPACITY = 0.3
  const BACKGROUND_BRIGHTNESS = 2.0
  const TEXT_OPACITY = 0.2
  const REFLECT_OPACITY = 0.3

  const TEXT_WIDTH = 5
  const INITIAL_DEPTH = -20
  const FINAL_DEPTH = -5
  const INITIAL_HEIGHT = 10
  const FINAL_HEIGHT = 1.5
  const CURVE_RADIUS = 2
  const TWEEN_DURATION = 5000
  
  const LOOP_INTERVAL = 1000
  
  const lines = textLines
  
  const cameraShader = 
  ` precision mediump float;

    varying vec2 texUv;
    uniform sampler2D sampler;
    uniform sampler2D textOverlay;
    uniform float backgroundOpacity;
    uniform float backgroundBrightness;
    uniform float textOpacity;

    void main() {
      vec4 cameraSample = texture2D(sampler, texUv);
      vec4 overlaySample = texture2D(textOverlay, vec2(texUv.x, 1.0-texUv.y));
      
      vec3 darkened = mix(vec3(0.0), cameraSample.rgb, backgroundOpacity);
      vec3 ligthened = cameraSample.rgb * backgroundBrightness;
      
      vec3 rgb = mix(darkened, ligthened, overlaySample.rgb) + overlaySample.rgb * textOpacity;

      gl_FragColor = vec4(rgb, 1.0);
    }`
    
  let textScene = null
  let renderTarget = null
  let textRender = null
  let textRenderReflect = null
  let customBounceOut = null
  let initDone = false
  
  const initXrScene = (scene, camera, renderer, canvases) => {
    // Enable shadows in the rednerer.
    // renderer.shadowMap.enabled = true

    XR8.GlTextureRenderer.configure({fragmentSource: cameraShader})

    // Load Textures
    ////////////////

    const textTexture = new THREE.CanvasTexture(canvases.text)
    const reflectTexture = new THREE.CanvasTexture(canvases.reflect)
    reflectTexture.flipY = false

    const width = window.innerWidth
    const height = window.innerHeight
    const pixelRatio = window.devicePixelRatio

    const effectiveWidth = width * pixelRatio
    const effectiveHeight = height * pixelRatio

    console.log(`Render Dimensions: ${width}x${height}@x${pixelRatio}`)

    
    // Main Text
    ////////////

    const textMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      map: textTexture,
      color: new THREE.Color(0xFFFFFF),
      opacity: 0,
      transparent: true,
    })
    
		const textGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH)
    const textMesh = new THREE.Mesh(textGeometry, textMaterial)
    textMesh.position.set(0, 1, INITIAL_DEPTH)

    textScene = new THREE.Scene()

    const rtOptions = {
      wrapS: THREE.ClampToEdgeWrapping,
      wrapT: THREE.ClampToEdgeWrapping,
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      type: THREE.UnsignedByteType,
      depthBuffer: false,
      stencilBuffer: false,
      generateMipmaps: false,
      shareDepthFrom: null,
    };

    renderTarget = new THREE.WebGLRenderTarget(effectiveWidth, effectiveHeight, rtOptions)


    // Reflection
    /////////////

    const reflectGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH)
    reflectGeometry.rotateX(-Math.PI / 2)

    const reflectMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      map: reflectTexture,
      transparent: true,
      opacity: 0,
      color: new THREE.Color(0xFFFFFF)
    })

    const reflectMesh = new THREE.Mesh(reflectGeometry, reflectMaterial)
    reflectMesh.position.set(0, 0, INITIAL_DEPTH + TEXT_WIDTH / 2)
    
    // Set the initial camera position relative to the scene we just laid out. This must be at a
    // height greater than y=0.
    camera.position.set(0, 2, 2)
    
    textScene.add(textMesh)
    scene.add(reflectMesh)

    customBounceOut = CustomEase.create('customBounceOut', 'M0,0 C0.162,0.172 0.358,0.963 0.366,1 0.374,0.985 0.366,0.878 0.436,0.878 0.504,0.878 0.509,0.983 0.516,1 0.532,0.948 0.544,0.916 0.58,0.916 0.62,0.916 0.616,0.952 0.632,0.998 0.644,0.95 0.671,0.944 0.682,0.944 0.689,0.944 0.72,0.94 0.734,1 0.748,0.964 0.762,0.962 0.776,0.962 0.792,0.962 0.808,0.964 0.828,1 0.846,0.976 0.854,0.974 0.872,0.974 0.892,0.974 0.91,1 0.91,1 0.91,1 0.922,0.984 0.95,0.984 0.978,0.984 1,1 1,1 ')
    animateIn(textMesh, reflectMesh)
  }

  const animateIn = (textMesh, reflectMesh) => {
    
    const startPosition = { angle: -Math.PI/2,  height: INITIAL_HEIGHT, depth: INITIAL_DEPTH }
    const tween = new TWEEN.Tween(startPosition)
      .to({ angle: Math.PI/2, height: FINAL_HEIGHT, depth: FINAL_DEPTH }, TWEEN_DURATION)
      .easing(customBounceOut)
      .onUpdate(() => {
        const x = Math.cos(startPosition.angle) * CURVE_RADIUS
        textMesh.position.x = x
        reflectMesh.position.x = x
        
        textMesh.position.y = startPosition.height
        textMesh.position.z = startPosition.depth
        reflectMesh.position.z = startPosition.depth + TEXT_WIDTH / 2
      })
      
    const startOpacity = { opacity: 0.0 }
    const opacityTween = new TWEEN.Tween(startOpacity)
      .to({ opacity: 1.0 }, 500)
      .onUpdate(() => {
        textMesh.material.opacity = startOpacity.opacity
      })

    const startOpacityReflect = { opacity: 0.0 }
    const opacityTweenReflect = new TWEEN.Tween(startOpacityReflect)
      .to({ opacity: 1.0 }, 1000)
      .delay(1000)
      .onUpdate(() => {
        reflectMesh.material.opacity = startOpacityReflect.opacity * REFLECT_OPACITY
      })
      
    const startFade = { opacity: 1.0 }
    const fadeTween = new TWEEN.Tween(startFade)
      .to({ opacity: 0.0 }, 500)
      .delay(1000)
      .onUpdate(() => {
        textMesh.material.opacity = startFade.opacity
        reflectMesh.material.opacity = startFade.opacity * REFLECT_OPACITY
      })
      .onComplete(() => {
        setTimeout(() => { animateIn(textMesh, reflectMesh) }, LOOP_INTERVAL);
      })

    tween.chain(fadeTween)
    
    tween.start()
    opacityTween.start()
    opacityTweenReflect.start()
  }

  const renderText = () => {
    const renderParams = {
      fontFamily: 'FuturaCondensedBold',
      lineHeight: 1,
    }
    renderParams.fontSize = textRender.calcFontFize(lines, {
      fontFamiliy: renderParams.fontFamily,
      lineHeight: renderParams.lineHeight,
      margin: 0.1})
    textRender.clear()
    textRender.render(lines, renderParams)

    textRender.blur(Math.round(renderParams.fontSize / 12))
    textRender.render(lines, renderParams)

    textRenderReflect.clear()
    textRenderReflect.render(lines, renderParams)
    textRenderReflect.blur(40)
  }
  
  // Return a camera pipeline module that adds scene elements on start.
  return {
    // Camera pipeline modules need a name. It can be whatever you want but must be unique within
    // your app.
    name: 'threejsinitscene',

    // onStart is called once when the camera feed begins. In this case, we need to wait for the
    // XR8.Threejs scene to be ready before we can access it to add content. It was created in
    // XR8.Threejs.pipelineModule()'s onStart method.
    onStart: ({canvas}) => {
      const {scene, camera, renderer} = XR8.Threejs.xrScene() // Get the 3js scene from XR8.Threejs.

			loadFont(() => {
			  const res = 1024
			  textRender = textRenderer(res, res)
        textRenderReflect = textRenderer(res, res)
        const canvases = {
          text: textRender.canvas,
          reflect: textRenderReflect.canvas }
        renderText()
        initXrScene(scene, camera, renderer, canvases)
        initDone = true
      })
      
      // Enable TWEEN animations.
      animate()
      function animate(time) {
        requestAnimationFrame(animate)
        TWEEN.update(time)
      }
      
      // Sync the xr controller's 6DoF position and camera paremeters with our scene.
      XR8.XrController.updateCameraProjectionMatrix(
        {origin: camera.position, facing: camera.quaternion})
      
      // Recenter content when the canvas is tapped.
      // canvas.addEventListener(
      //  'touchstart', (e) => { e.touches.length == 1 && XR8.XrController.recenter() }, true)
    },
    
    onUpdate: ({frameStartResult, processGpuResult}) => {
      if (initDone) {
        const {scene, camera, renderer} = XR8.Threejs.xrScene()
      
        const currentTarget = renderer.getRenderTarget()
        renderer.setClearColor(new THREE.Color(0x000000), 0.0);
        renderer.setRenderTarget(renderTarget)
        renderer.clear();
        renderer.render(textScene, camera)
        renderer.setRenderTarget(currentTarget);
  
        // Configure Camera Shader
        
        const {shader} = processGpuResult.gltexturerenderer
        const {GLctx} = frameStartResult
        const p = XR8.GlTextureRenderer.getGLctxParameters(GLctx, [GLctx.TEXTURE0])
        GLctx.useProgram(shader)
        
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'backgroundOpacity'), BACKGROUND_OPACITY)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'backgroundBrightness'), BACKGROUND_BRIGHTNESS)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'textOpacity'), TEXT_OPACITY)
        
        const textureProperties = renderer.properties.get(renderTarget.texture);
        GLctx.activeTexture(GLctx.TEXTURE1);
        GLctx.bindTexture(GLctx.TEXTURE_2D, textureProperties.__webglTexture);
        GLctx.uniform1i(GLctx.getUniformLocation(shader, 'textOverlay'), 1);
        
        XR8.GlTextureRenderer.setGLctxParameters(GLctx, p)
      }
    }
  }
}
