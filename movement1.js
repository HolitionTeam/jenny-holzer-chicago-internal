import {loadFont, textRenderer} from './text-renderer.js'

export const pipelineModule1 = (textLines) => {
  const BACKGROUND_OPACITY = 0.3
  const BACKGROUND_BRIGHTNESS = 2.0
  const TEXT_OPACITY = 0.2
  const REFLECT_OPACITY = 0.3

  const TEXT_WIDTH = 5
  const INITIAL_DEPTH = -50
  const FINAL_DEPTH = 3
  const TWEEN_DURATION = 10000
  
  const LOOP_INTERVAL = 1000
  
  const INITIAL_BLOOM = 2
  const FINAL_BLOOM = 0

  const BLUR_INITIAL_OFFSET = 0

  const lines = textLines
  
  const cameraShader = 
  ` precision mediump float;

    varying vec2 texUv;
    uniform sampler2D sampler;
    uniform sampler2D textOverlay;
    uniform float backgroundOpacity;
    uniform float backgroundBrightness;
    uniform float textOpacity;

    void main() {
      vec4 cameraSample = texture2D(sampler, texUv);
      vec4 overlaySample = texture2D(textOverlay, vec2(texUv.x, 1.0-texUv.y));
      
      vec3 darkened = mix(vec3(0.0), cameraSample.rgb, backgroundOpacity);
      vec3 ligthened = cameraSample.rgb * backgroundBrightness;
      
      vec3 rgb = mix(darkened, ligthened, overlaySample.rgb) + overlaySample.rgb * textOpacity;

      gl_FragColor = vec4(rgb, 1.0);
    }`
    
  let textScene = null
  let renderTarget = null
  let composer = null
  let bloomPass = null
  let textRender = null
  let textRenderReflect = null
  let blurOffset = BLUR_INITIAL_OFFSET
  let initDone = false

  const initXrScene = (scene, camera, renderer, canvases) => {
    // Enable shadows in the rednerer.
    // renderer.shadowMap.enabled = true

    XR8.GlTextureRenderer.configure({fragmentSource: cameraShader})

    // Load Textures
    ////////////////

    const textTexture = new THREE.CanvasTexture(canvases.text)
    const reflectTexture = new THREE.CanvasTexture(canvases.reflect)
    reflectTexture.flipY = false

    const width = window.innerWidth
    const height = window.innerHeight
    const pixelRatio = window.devicePixelRatio

    const effectiveWidth = width * pixelRatio
    const effectiveHeight = height * pixelRatio

    console.log(`Render Dimensions: ${width}x${height}@x${pixelRatio}`)

    
    // Main Text
    ////////////

    const textMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      map: textTexture,
      color: new THREE.Color(0xFFFFFF),
      opacity: 0,
      transparent: true,
    })
    
		const textGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH)
    const textMesh = new THREE.Mesh(textGeometry, textMaterial)
    textMesh.position.set(0, 2, INITIAL_DEPTH)

    textScene = new THREE.Scene()

    /* const rtOptions = {
      wrapS: THREE.ClampToEdgeWrapping,
      wrapT: THREE.ClampToEdgeWrapping,
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      type: THREE.UnsignedByteType,
      depthBuffer: false,
      stencilBuffer: false,
      generateMipmaps: false,
      shareDepthFrom: null,
    };

    renderTarget = new THREE.WebGLRenderTarget(effectiveWidth, effectiveHeight, rtOptions) */


    // Effects
    //////////

		composer = new THREE.EffectComposer(renderer)
		composer.setSize(width, height)

    const renderPass = new THREE.RenderPass(textScene, camera)
		composer.addPass(renderPass)

    const dimensions = new THREE.Vector2(effectiveWidth, effectiveHeight)
    bloomPass = new THREE.UnrealBloomPass(dimensions, 0, INITIAL_BLOOM, 0)
		bloomPass.threshold = 0
		bloomPass.strength = INITIAL_BLOOM
		bloomPass.radius = 0
		composer.addPass(bloomPass)
      
    const copyPass = new THREE.ShaderPass(THREE.CopyShader)
		composer.addPass(copyPass)
		
    // Reflection
    /////////////

    const reflectGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH)
    reflectGeometry.rotateX(-Math.PI / 2)

    const reflectMaterial = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      map: reflectTexture,
      transparent: true,
      opacity: 0.0,
      color: new THREE.Color(0xFFFFFF)
    })

    const reflectMesh = new THREE.Mesh(reflectGeometry, reflectMaterial)
    reflectMesh.position.set(0, 0, INITIAL_DEPTH + TEXT_WIDTH / 2)
    
    // Set the initial camera position relative to the scene we just laid out. This must be at a
    // height greater than y=0.
    camera.position.set(0, 2, 2)
    
    textScene.add(textMesh)
    scene.add(reflectMesh)
    animateIn(textMesh, reflectMesh)
  }
  
  const bezierEasing = BezierEasing(.7,1.27,.83,.52)
  const customEasing = (k) => {
    return bezierEasing(k)
  }
  
  const animateIn = (textMesh, reflectMesh) => {
    textMesh.visible = true
    reflectMesh.visible = true
    
    const startPosition = { position: INITIAL_DEPTH }
    
    const tween = new TWEEN.Tween(startPosition)
      .to({ position: FINAL_DEPTH }, TWEEN_DURATION)
      .easing(customEasing)
      .onUpdate((k) => {
        textMesh.position.z = startPosition.position
        reflectMesh.position.z = startPosition.position + TEXT_WIDTH / 2
      })
      .onComplete(() => {
        reflectMesh.material.opacity = 0
        textMesh.material.opacity = 0
        bloomPass.strength = INITIAL_BLOOM
        blurOffset = BLUR_INITIAL_OFFSET
        textMesh.visible = false
        reflectMesh.visible = false
        setTimeout(() => { animateIn(textMesh, reflectMesh) }, LOOP_INTERVAL);
      })
    
    const startOpacity = { opacity: 0.0 }
    const reflectTween = new TWEEN.Tween(startOpacity)
      .to({ opacity: REFLECT_OPACITY }, 2000)
      .delay(3000)
      .onUpdate(() => {
        reflectMesh.material.opacity = startOpacity.opacity
      })
      
    const startOpacityText = { opacity: 0.0 }
    const textTween = new TWEEN.Tween(startOpacityText)
      .to({ opacity: 1.0 }, 500)
      .onUpdate(() => {
        textMesh.material.opacity = startOpacityText.opacity
      })

    const startBloom = { bloom: INITIAL_BLOOM }
    const bloomTween = new TWEEN.Tween(startBloom)
      .to({ bloom: FINAL_BLOOM }, 3000)
      .easing(TWEEN.Easing.Quadratic.InOut)
      .onUpdate(() => {
        bloomPass.strength = startBloom.bloom
      })

    /* const startBlur = { blur: BLUR_INITIAL_OFFSET }
    const blurTween = new TWEEN.Tween(startBlur)
      .to({ blur: 0 }, 3000)
      .easing(TWEEN.Easing.Quadratic.Out)
      .onUpdate(() => {
        blurOffset = startBlur.blur
      }) */
      
    tween.start()
    reflectTween.start()
    textTween.start()
    // blurTween.start()
    bloomTween.start()
  }

  const renderText = () => {
    const renderParams = {
      fontFamily: 'FuturaCondensedBold',
      lineHeight: 1,
    }
    renderParams.fontSize = textRender.calcFontFize(lines, {
      fontFamiliy: renderParams.fontFamily,
      lineHeight: renderParams.lineHeight,
      margin: 0.1})
    textRender.clear()
    textRender.render(lines, renderParams)

    textRender.blur(Math.round(renderParams.fontSize / 12))
    textRender.render(lines, renderParams)

    textRenderReflect.clear()
    textRenderReflect.render(lines, renderParams)
    textRenderReflect.blur(40)
  }
  
  // Return a camera pipeline module that adds scene elements on start.
  return {
    // Camera pipeline modules need a name. It can be whatever you want but must be unique within
    // your app.
    name: 'threejsinitscene',

    // onStart is called once when the camera feed begins. In this case, we need to wait for the
    // XR8.Threejs scene to be ready before we can access it to add content. It was created in
    // XR8.Threejs.pipelineModule()'s onStart method.
    onStart: ({canvas}) => {
      const {scene, camera, renderer} = XR8.Threejs.xrScene() // Get the 3js scene from XR8.Threejs.

			loadFont(() => {
			  const res = 1024
			  textRender = textRenderer(res, res)
        textRenderReflect = textRenderer(res, res)
        const canvases = {
          text: textRender.canvas,
          reflect: textRenderReflect.canvas }
        renderText()
        initXrScene(scene, camera, renderer, canvases)
        initDone = true
      })
      
      // Enable TWEEN animations.
      animate()
      function animate(time) {
        requestAnimationFrame(animate)
        TWEEN.update(time)
      }
      
      // Sync the xr controller's 6DoF position and camera paremeters with our scene.
      XR8.XrController.updateCameraProjectionMatrix(
        {origin: camera.position, facing: camera.quaternion})
      
      // Recenter content when the canvas is tapped.
      // canvas.addEventListener(
      //  'touchstart', (e) => { e.touches.length == 1 && XR8.XrController.recenter() }, true)
    },
    
    onUpdate: ({frameStartResult, processGpuResult}) => {
      if (initDone) {
        const {scene, camera, renderer} = XR8.Threejs.xrScene()

        composer.render()

        /* const currentTarget = renderer.getRenderTarget()
        renderer.setClearColor(new THREE.Color(0x000000), 0.0);
        renderer.setRenderTarget(renderTarget)
        renderer.clear();
        renderer.render(textScene, camera)
        renderer.setRenderTarget(currentTarget); */

        // Configure Camera Shader
        
        const {shader} = processGpuResult.gltexturerenderer
        const {GLctx} = frameStartResult
        const p = XR8.GlTextureRenderer.getGLctxParameters(GLctx, [GLctx.TEXTURE0])
        GLctx.useProgram(shader)
        
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'backgroundOpacity'), BACKGROUND_OPACITY)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'backgroundBrightness'), BACKGROUND_BRIGHTNESS)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'textOpacity'), TEXT_OPACITY)
        
        const textureProperties = renderer.properties.get(composer.readBuffer.texture);
        // const textureProperties = renderer.properties.get(renderTarget.texture);
        GLctx.activeTexture(GLctx.TEXTURE1);
        GLctx.bindTexture(GLctx.TEXTURE_2D, textureProperties.__webglTexture);
        GLctx.uniform1i(GLctx.getUniformLocation(shader, 'textOverlay'), 1);
        
        XR8.GlTextureRenderer.setGLctxParameters(GLctx, p)
      }

      // renderText()
    }
  }
}
