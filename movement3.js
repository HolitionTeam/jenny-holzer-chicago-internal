import {loadFont, renderTextSplit} from './text-renderer.js'

export const pipelineModule3 = (textLines) => {
  const BACKGROUND_OPACITY = 0.3
  const BACKGROUND_BRIGHTNESS = 2.0
  const TEXT_OPACITY = 0.2
  const REFLECT_OPACITY = 0.3
  
  const TEXT_WIDTH = 5
  const INITIAL_DEPTH = -20
  const FINAL_DEPTH = -5
  const TWEEN_DURATION = 3000
  
  const LOOP_INTERVAL = 1000
  
  const INITIAL_BLOOM = 1
  const FINAL_BLOOM = 0

  const lines = textLines

  const cameraShader = 
  ` precision mediump float;

    varying vec2 texUv;
    uniform sampler2D sampler;
    uniform sampler2D textOverlay1;
    uniform sampler2D textOverlay2;
    uniform sampler2D textOverlay3;
    uniform float backgroundOpacity;
    uniform float backgroundBrightness;
    uniform float textOpacity;

    void main() {
      vec2 uv = vec2(texUv.x, 1.0-texUv.y);
    
      vec4 cameraSample = texture2D(sampler, texUv);
      vec4 overlaySample = vec4(0.0);
      overlaySample += texture2D(textOverlay1, uv);
      overlaySample += texture2D(textOverlay2, uv);
      overlaySample += texture2D(textOverlay3, uv);

      vec3 darkened = mix(vec3(0.0), cameraSample.rgb, backgroundOpacity);
      vec3 ligthened = cameraSample.rgb * backgroundBrightness;
      
      vec3 rgb = mix(darkened, ligthened, overlaySample.rgb) + overlaySample.rgb * textOpacity;

      gl_FragColor = vec4(rgb, 1.0);
    }`
    
  let composers = []
  let initDone = false

  const initXrScene = (scene, camera, renderer, canvases) => {
    // Enable shadows in the rednerer.
    // renderer.shadowMap.enabled = true

    XR8.GlTextureRenderer.configure({fragmentSource: cameraShader})

    // Load Textures
    ////////////////

    const textTextures = []
    for (const canvas of canvases.text) {
      const textTexture = new THREE.CanvasTexture(canvas)
      textTextures.push(textTexture)
    }
    const reflectTextures = []
    for (const canvas of canvases.reflect) {
      const reflectTexture = new THREE.CanvasTexture(canvas)
      reflectTexture.flipY = false
      reflectTextures.push(reflectTexture)
    }

    const width = window.innerWidth
    const height = window.innerHeight
    const pixelRatio = window.devicePixelRatio

    const effectiveWidth = width * pixelRatio
    const effectiveHeight = height * pixelRatio

    console.log(`Render Dimensions: ${width}x${height}@x${pixelRatio}`)

    const meshGroups = []
    const textScenes = []
    
    // Main Text
    ////////////

    const textMeshes = []

    for (const textTexture of textTextures) {
      const textMaterial = new THREE.MeshBasicMaterial({
        side: THREE.DoubleSide,
        map: textTexture,
        color: new THREE.Color(0xFFFFFF),
        opacity: 0,
        transparent: true,
      })
      
  		const textGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH)
      const textMesh = new THREE.Mesh(textGeometry, textMaterial)
      textMesh.position.set(0, 2, INITIAL_DEPTH)
      textMeshes.push(textMesh)
    }

    // Reflection
    /////////////
    
    const reflectMeshes = []
    
    for (const reflectTexture of reflectTextures) {
      const reflectGeometry = new THREE.PlaneGeometry(TEXT_WIDTH, TEXT_WIDTH)
      reflectGeometry.rotateX(-Math.PI / 2)
  
      const reflectMaterial = new THREE.MeshBasicMaterial({
        side: THREE.DoubleSide,
        map: reflectTexture,
        transparent: true,
        depthTest: false,
        opacity: 0,
        color: new THREE.Color(0xFFFFFF)
      })
  
      const reflectMesh = new THREE.Mesh(reflectGeometry, reflectMaterial)
      reflectMesh.position.set(0, 0, INITIAL_DEPTH + TEXT_WIDTH / 2)

      reflectMeshes.push(reflectMesh)
      
      const textScene = new THREE.Scene()
      textScenes.push(textScene)
    }
    
    // Effects
    //////////
  
    let i = 0
    for (const textScene of textScenes) {
  		const composer = new THREE.EffectComposer(renderer)
  		composer.setSize(width, height)
  
      const renderPass = new THREE.RenderPass(textScene, camera)
  		composer.addPass(renderPass)
  
      const dimensions = new THREE.Vector2(effectiveWidth, effectiveHeight)
      const bloomPass = new THREE.UnrealBloomPass(dimensions, 0, INITIAL_BLOOM, 0)
  		bloomPass.threshold = 0
  		bloomPass.strength = INITIAL_BLOOM
  		bloomPass.radius = 0
  		composer.addPass(bloomPass)
        
      const copyPass = new THREE.ShaderPass(THREE.CopyShader)
  		composer.addPass(copyPass)
  		
  		composers.push(composer)

      const group = new THREE.Group()
      group.add(textMeshes[i])
      group.add(reflectMeshes[i])
      group.bloomPass = bloomPass
      meshGroups.push(group)
      
  		i++
    }

    // Set the initial camera position relative to the scene we just laid out. This must be at a
    // height greater than y=0.
    camera.position.set(0, 2, 2)
    
    let j = 0
    for (const meshGroup of meshGroups) {
      textScenes[j].add(meshGroup)
      j++
    }
    animateIn(meshGroups)
  }
  
  const setMeshesOpacity = (meshGroups, opacity) => {
    for (const meshGroup of meshGroups) {
      meshGroup.children[0].material.opacity = opacity
      meshGroup.children[1].material.opacity = opacity * REFLECT_OPACITY
    }
  }
  
  const animateIn = (meshGroups) => {
    const tweens = []

    let delay = 0;
    for (const meshGroup of meshGroups) {
      const startOpacity = { opacity: 0.0 }
      const opacityTween = new TWEEN.Tween(startOpacity)
        .to({ opacity: 1.0 }, 500)
        .onUpdate(() => {
           meshGroup.children[0].material.opacity = startOpacity.opacity
           meshGroup.children[1].material.opacity = startOpacity.opacity * REFLECT_OPACITY
        })
      
      const startBloom = { bloom: INITIAL_BLOOM }
      const bloomTween = new TWEEN.Tween(startBloom)
        .to({ bloom: FINAL_BLOOM }, 3000)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onUpdate(() => {
          meshGroup.bloomPass.strength = startBloom.bloom
        })
        
      const startPosition = { position: INITIAL_DEPTH }
      const tween = new TWEEN.Tween(startPosition)
        .to({ position: FINAL_DEPTH }, TWEEN_DURATION)
        .easing(TWEEN.Easing.Quadratic.Out)
        .delay(delay)
        .onStart(() => {
          bloomTween.start()
          opacityTween.start()
        })
        .onUpdate(() => {
          meshGroup.children[0].position.z = startPosition.position
          meshGroup.children[1].position.z = startPosition.position + TEXT_WIDTH / 2
        })
      tweens.push(tween)
      delay += TWEEN_DURATION / 2
    }
    
    const startFade = { opacity: 1.0 }
    const opacityTween = new TWEEN.Tween(startFade)
      .to({ opacity: 0.0 }, 500)
      .delay(500)
      .onUpdate(() => {
        setMeshesOpacity(meshGroups, startFade.opacity)
      })
      .onComplete(() => {
        setTimeout(() => { animateIn(meshGroups) }, LOOP_INTERVAL)
      })
    
    tweens[tweens.length - 1].chain(opacityTween)
      
    for (let i = 0; i < tweens.length; i++) {
      tweens[i].start()
    }      
  }
  
  // Return a camera pipeline module that adds scene elements on start.
  return {
    // Camera pipeline modules need a name. It can be whatever you want but must be unique within
    // your app.
    name: 'threejsinitscene',

    // onStart is called once when the camera feed begins. In this case, we need to wait for the
    // XR8.Threejs scene to be ready before we can access it to add content. It was created in
    // XR8.Threejs.pipelineModule()'s onStart method.
    onStart: ({canvas}) => {
      const {scene, camera, renderer} = XR8.Threejs.xrScene() // Get the 3js scene from XR8.Threejs.
			
      loadFont(() => {
        const canvases = renderTextSplit(lines)
        initXrScene(scene, camera, renderer, canvases)
        initDone = true
      })

      // Enable TWEEN animations.
      animate()
      function animate(time) {
        requestAnimationFrame(animate)
        TWEEN.update(time)
      }
      
      // Sync the xr controller's 6DoF position and camera paremeters with our scene.
      XR8.XrController.updateCameraProjectionMatrix(
        {origin: camera.position, facing: camera.quaternion})
      
      // Recenter content when the canvas is tapped.
      // canvas.addEventListener(
      //  'touchstart', (e) => { e.touches.length == 1 && XR8.XrController.recenter() }, true)
    },
    
    onUpdate: ({frameStartResult, processGpuResult}) => {
      if (initDone) {
        const {scene, camera, renderer} = XR8.Threejs.xrScene()
      
        for (const composer of composers) {
          composer.render()
        }
  
        // Configure Camera Shader
        
        const {shader} = processGpuResult.gltexturerenderer
        const {GLctx} = frameStartResult
        const p = XR8.GlTextureRenderer.getGLctxParameters(GLctx, [GLctx.TEXTURE0])
        GLctx.useProgram(shader)
        
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'backgroundOpacity'), BACKGROUND_OPACITY)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'backgroundBrightness'), BACKGROUND_BRIGHTNESS)
        GLctx.uniform1f(GLctx.getUniformLocation(shader, 'textOpacity'), TEXT_OPACITY)
        
        let i = 0;
        for (const composer of composers) {
          const textureProperties = renderer.properties.get(composer.readBuffer.texture);
          GLctx.activeTexture(GLctx.TEXTURE1 + i);
          GLctx.bindTexture(GLctx.TEXTURE_2D, textureProperties.__webglTexture);
          GLctx.uniform1i(GLctx.getUniformLocation(shader, `textOverlay${i+1}`), i+1);
          i++;
        }
  
        XR8.GlTextureRenderer.setGLctxParameters(GLctx, p)
      }
    }
  }
}
